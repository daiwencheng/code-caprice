package array;

import java.util.Arrays;

/**
 * 有序数组的平方
 * 问：
 *  给你一个按 非递减顺序 排序的整数数组 nums，返回 每个数字的平方 组成的新数组，要求也按 非递减顺序 排序。
 * 示例 1:
 *  输入：nums = [-4,-1,0,3,10]
 *  输出：[0,1,9,16,100]
 *  解释：平方后，数组变为 [16,1,0,9,100]，排序后，数组变为 [0,1,9,16,100]
 * 思路：
 * <br>
 * <img src="https://code-thinking.cdn.bcebos.com/gifs/977.%E6%9C%89%E5%BA%8F%E6%95%B0%E7%BB%84%E7%9A%84%E5%B9%B3%E6%96%B9.gif"/>
 * <br>
 * 时空：
 *  时间复杂度：O(n)
 *  空间复杂度：O(n)
 */
public class ArraySquare {
    public static void main(String[] args) {
        int[] nums = {-4, -1, 0, 3, 10};
        int[] result = arraySquare(nums);
        System.out.println("result = " + Arrays.toString(result));
    }

    private static int[] arraySquare(int[] nums) {
        int right = nums.length - 1;
        int left = 0;
        int m = nums.length - 1;
        int[] result = new int[nums.length];

        while (left <= right) {
            int leftNum = nums[left] * nums[left];
            int rightNum = nums[right] * nums[right];
            if (leftNum > rightNum) {
                result[m] = leftNum;
                left++;
            } else {
                result[m] = rightNum;
                right--;
            }
            m--;
        }
        return result;
    }
}
