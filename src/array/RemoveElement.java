package array;

import java.util.Arrays;

/**
 * 移除元素
 * 问：
 *  给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
 *  不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并原地修改输入数组。
 *  元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。
 * 前提：
 * 示例 1:
 *  输入：nums = [3,2,2,3], val = 3
 *  输出：2, nums = [2,2]
 *  解释：函数应该返回新的长度 2, 并且 nums 中的前两个元素均为 2。你不需要考虑数组中超出新长度后面的元素。例如，函数返回的新长度为 2 ，而 nums = [2,2,3,3] 或 nums = [2,2,0,0]，也会被视作正确答案。
 * 思路：
 *  双指针法
 *  使用快指针进行遍历，使用快慢指针进行元素交换
 *  <br>
 *  <img src="https://code-thinking.cdn.bcebos.com/gifs/27.%E7%A7%BB%E9%99%A4%E5%85%83%E7%B4%A0-%E6%9A%B4%E5%8A%9B%E8%A7%A3%E6%B3%95.gif">
 *  <br>
 * 时空：
 *  时间复杂度：O(n)
 *  空间复杂度：O(1)
 */
public class RemoveElement {
    public static void main(String[] args) {
        int[] nums = {0,1,2,2,3,0,4,2};
        int val = 2;
        int length = removeElement(nums, val);
        System.out.println("length = " + length);
        System.out.println("nums = " + Arrays.toString(nums));

    }

    public static int removeElement(int[] nums, int val) {
        int left = nums.length-1;
        int right = nums.length-1;
        while (right >= 0) {
            if (nums[right] == val) {
                nums[right] = nums[left];
                left--;
            }
            right--;
        }
        return left+1;
    }
}
